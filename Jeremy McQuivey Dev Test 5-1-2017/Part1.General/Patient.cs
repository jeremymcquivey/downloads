﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Part1.General
{
    public partial class Patient : IValidatedModel, IDisposable
    {
        private bool _isDisposed = false;

        #region public properties

        /// <summary>
        /// Internal unique identifier for the patient
        /// </summary>
        public int PatientId { set; get; }
        
        /// <summary>
        /// Number assigned by the facility as a patient identifier.
        /// </summary>
        public string MedicalRecordNumber { set; get; }

        /// <summary>
        /// Given name of the patient
        /// </summary>
        public string FirstName { set; get; }

        /// <summary>
        /// Surname of the patient.
        /// </summary>
        public string LastName { set; get; }

        /// <summary>
        /// Street Address of patient
        /// </summary>
        public string Address1 { set; get; }

        /// <summary>
        /// Secondary address information of patient.
        /// </summary>
        public string Address2 { set; get; }

        /// <summary>
        /// City of patient's residence
        /// </summary>
        public string City { set; get; }

        /// <summary>
        /// State of patient's residence
        /// </summary>
        public string State { set; get; }

        /// <summary>
        /// Postal code of patient's residence
        /// </summary>
        public string Zip { set; get; }

        /// <summary>
        /// List of associated insurance policies
        /// </summary>
        public IEnumerable<InsurancePolicy> Policies;

        #endregion 

        /// <summary>
        /// Validates the data provided about the patient.
        /// </summary>
        /// <returns>whether or not the data is valid.</returns>
        public bool Validate()
        {
            if(_isDisposed)
            {
                return false;
            }

            var hasBasicInfo =  !string.IsNullOrEmpty(MedicalRecordNumber) &&
                                !string.IsNullOrEmpty(FirstName) &&
                                !string.IsNullOrEmpty(LastName) &&
                                !string.IsNullOrEmpty(Address1);

            var hasInsurancePolicy = null != Policies &&
                                     Policies.Any();

            return hasBasicInfo && hasInsurancePolicy;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// This is the really ugly way of doing it.
        /// </summary>
        /// <returns>a friendly string describing the patient.</returns>
        public string GetDisplayText1()
        {
            var builder = new StringBuilder(FirstName.Trim() + " " + LastName.Trim());

            if(Policies.Any())
            {
                var policy = Policies.First();
                builder.Append(" - " + policy.ProviderName + " " + policy.PolicyNumber);
            }

            return builder.ToString();
        }

        /// <summary>
        /// this is still an ok way of doing it if your build agent doesn't support C#6
        /// </summary>
        /// <returns>a friendly string describing the patient.</returns>
        public string GetDisplayText2()
        {
            var nameStr = string.Format("{0} {1}", FirstName.Trim(), LastName.Trim());

            var policy = Policies.FirstOrDefault();
            var policyStr = string.Empty;

            if(default(InsurancePolicy) != policy)
            {
                policyStr = string.Format("{0} {1}", policy.ProviderName, policy.PolicyNumber);
            }

            return string.Format(!string.IsNullOrEmpty(policyStr) ? "{0} - {1}" : "{0}", nameStr, policyStr);
        }

        /// <summary>
        /// this is a really cool way of doing it if you have C#6 or higher.
        /// </summary>
        /// <returns></returns>
        public string GetDisplayText3()
        {
            var nameStr = $"{FirstName.Trim()} {LastName.Trim()}";

            var policy = Policies.FirstOrDefault();
            var policyStr = string.Empty;

            if(default(InsurancePolicy) != policy)
            {
                policyStr = $"{policy.ProviderName} {policy.PolicyNumber}";
            }

            return string.IsNullOrEmpty(policyStr) ? $"{nameStr}": $"{nameStr} - {policyStr}";
        }

        private void Dispose(bool isDisposing)
        {
            if(_isDisposed)
            {
                return;
            }

            if(isDisposing)
            {
                PatientId = default(int);
                Address1 = null;
                Address2 = null;
                City = null;
                FirstName = null;
                LastName = null;
                MedicalRecordNumber = null;
                State = null;
                Zip = null;
            }
        }
    }
}
