﻿using Newtonsoft.Json;

namespace Part1.General
{
    public static class PatientExtensions
    {
        public static string ToJson(this Patient patient)
        {
            return JsonConvert.SerializeObject(patient);
        }
    }
}
