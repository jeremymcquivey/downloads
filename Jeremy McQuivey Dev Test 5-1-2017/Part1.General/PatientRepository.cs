﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Part1.General
{
    public class PatientRepository
    {
        private List<Patient> _data = new List<Patient>();

        public IEnumerable<Patient> Search(Func<Patient, bool> filter)
        {
            return _data.Where(filter);
        }
        
        public void Add(Patient patient)
        {
            _data.Add(patient);
        }
    }
}
