﻿namespace Part1.General
{
    public interface IValidatedModel
    {
        bool Validate();
    }
}