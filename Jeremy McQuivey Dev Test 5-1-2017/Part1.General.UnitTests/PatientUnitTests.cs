﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Part1.General.UnitTests
{
    [TestClass]
    public class PatientUnitTests
    {
        private Patient _patient;

        [TestInitialize]
        public void TI()
        {
            _patient = new Patient()
            {
                Address1 = "911 Emergency Blvd",
                Address2 = "Suite 95Q",
                City = "Brentwood",
                State = "TN",
                Zip = "37027",
                FirstName = "Half",
                LastName = "Man",
                MedicalRecordNumber = "ABC123",
                PatientId = 0,
                Policies = new List<InsurancePolicy>()
                {
                    new InsurancePolicy()
                    {
                        PatientId = 0,
                        PolicyNumber = "ABCI123",
                        ProviderName = "A Mediocre Insurance Agency, Inc."
                    }
                }
            };
        }

        [TestMethod]
        public void PatientObjectCorrectlyPopulated()
        {
            Assert.IsTrue(_patient.Validate(), "Validation of the patient object failed when it was expected to pass.");
        }

        [TestMethod]
        public void PatientObjectMissingFirstName()
        {
            _patient.FirstName = string.Empty;
            Assert.IsFalse(_patient.Validate(), "Validation of the patient object passed when it was expected to fail.");
        }

        [TestMethod]
        public void PatientObjectMissingLastName()
        {
            _patient.LastName = string.Empty;
            Assert.IsFalse(_patient.Validate(), "Validation of the patient object passed when it was expected to fail.");
        }

        [TestMethod]
        public void PatientObjectMissingRecordNumber()
        {
            _patient.MedicalRecordNumber = string.Empty;
            Assert.IsFalse(_patient.Validate(), "Validation of the patient object passed when it was expected to fail.");
        }

        [TestMethod]
        public void PatientObjetMissingAddress()
        {
            _patient.Address1 = string.Empty;
            Assert.IsFalse(_patient.Validate(), "Validation of the patient object passed when it was expected to fail.");
        }

        [TestMethod]
        public void PatientObjectMissingPolicy()
        {
            _patient.Policies = new List<InsurancePolicy>();
            Assert.IsFalse(_patient.Validate(), "Validation of the patient object passed when it was expected to fail.");
        }

        [TestMethod]
        public void PatientObjectMissingOptionalData()
        {
            _patient.Address2 = string.Empty;
            _patient.State = string.Empty;
            _patient.Zip = string.Empty;

            Assert.IsTrue(_patient.Validate(), "Validation of the patient object failed when it was expected to pass.");
        }

        [TestMethod]
        public void PatientObjectString1WithPolicy()
        {
            Assert.AreEqual("Half Man - A Mediocre Insurance Agency, Inc. ABCI123", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectString1WithoutPolicy()
        {
            _patient.Policies = new List<InsurancePolicy>();
            Assert.AreEqual("Half Man", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectString2WithPolicy()
        {
            Assert.AreEqual("Half Man - A Mediocre Insurance Agency, Inc. ABCI123", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectString2WithoutPolicy()
        {
            _patient.Policies = new List<InsurancePolicy>();
            Assert.AreEqual("Half Man", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectString3WithPolicy()
        {
            Assert.AreEqual("Half Man - A Mediocre Insurance Agency, Inc. ABCI123", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectString3WithoutPolicy()
        {
            _patient.Policies = new List<InsurancePolicy>();
            Assert.AreEqual("Half Man", _patient.GetDisplayText1(), "String method 1 is incorrectly formatted");
        }

        [TestMethod]
        public void PatientObjectToJson()
        {
            var json = _patient.ToJson();
            Assert.IsFalse(string.IsNullOrEmpty(json));
        }
    }
}
