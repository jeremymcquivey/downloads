﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Part1.General.UnitTests
{
    [TestClass]
    public class RepositoryUnitTests
    {
        private PatientRepository _repo;

        [TestInitialize]
        public void TI()
        {
            _repo = new PatientRepository();
            _repo.Add(new Patient() { FirstName = "Baffling", LastName = "Copy", MedicalRecordNumber = "ABC123" });
            _repo.Add(new Patient() { FirstName = "Peach", LastName = "Guru", MedicalRecordNumber = "ABC1234" });
            _repo.Add(new Patient() { FirstName = "Gummy", LastName = "Bear", MedicalRecordNumber = "ABC12345" });
        }

        [TestMethod]
        public void TestSearchMethod()
        {
            var results = _repo.Search(x => x.LastName == "Bear");
            Assert.AreEqual(1, results.Count());
            Assert.AreEqual("ABC12345", results.First().MedicalRecordNumber);
        }

        [TestMethod]
        public void TestSearchMethod2()
        {
            var results = _repo.Search(x => true);
            Assert.AreEqual(3, results.Count());
        }
    }
}
