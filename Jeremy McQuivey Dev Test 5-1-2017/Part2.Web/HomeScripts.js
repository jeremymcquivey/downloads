$(document).ready(function(){
    
    $(".color.clickable").click(function () {
        var elementColor = $(this).attr("class").split(" ")[1];
        $("#selectedColor").removeClass().addClass('color ' + elementColor);

        var txtContent = $(this).html();
        $("#selectedColor").html(txtContent);
    });
});