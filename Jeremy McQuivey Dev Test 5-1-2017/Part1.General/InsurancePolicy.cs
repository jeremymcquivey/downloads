﻿namespace Part1.General
{
    public sealed class InsurancePolicy : IValidatedModel
    {
        public string ProviderName { set; get; }

        public string PolicyNumber { set; get; }

        public int PatientId { set; get; }

        public bool Validate()
        {
            return !string.IsNullOrEmpty(ProviderName) &&
                   !string.IsNullOrEmpty(PolicyNumber);
        }
    }
}
