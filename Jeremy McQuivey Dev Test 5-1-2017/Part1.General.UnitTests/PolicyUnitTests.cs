﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Part1.General.UnitTests
{
    [TestClass]
    public class PolicyUnitTests
    {
        private InsurancePolicy _policy;

        [TestInitialize]
        public void TI()
        {
            _policy = new InsurancePolicy()
            {
                PolicyNumber = "ABCP123",
                ProviderName = "Good Insurance Company, Inc."
            };
        }

        [TestMethod]
        public void InsurancePolicyPopulatedCorrectly()
        {
            Assert.IsTrue(_policy.Validate(), "Policy validation failed when it was expected to pass.");
        }

        [TestMethod]
        public void InsurancePolicyMissingPolicyNumber()
        {
            _policy.PolicyNumber = string.Empty;
            Assert.IsFalse(_policy.Validate(), "Policy validation succeeded when it was expected to fail.");
        }

        [TestMethod]
        public void InsurancePolicyMissingProviderName()
        {
            _policy.ProviderName = string.Empty;
            Assert.IsFalse(_policy.Validate(), "Policy validation succeeded when it was expected to fail.");
        }
    }
}
